import React, { Fragment } from "react";
import { PropTypes } from "prop-types";

export function Photos(props) {
  const photos = props.data.slice(0, 10);

  return (
    <Fragment>
      <ul className="photos-list">
        {photos.map((photo) => {
          return (
            <li key={photo.id}>
              <img src={photo.url} alt={photo.title} style={{ width: 150 }} />
            </li>
          );
        })}
      </ul>
    </Fragment>
  );
}

Photos.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      alt: PropTypes.string,
      url: PropTypes.string,
    })
  ),
};
