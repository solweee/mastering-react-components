import React, { Fragment } from "react";
import ReactDOM from "react-dom/client";
import { PropTypes } from "prop-types";
import "./index.css";
import { data } from "./Data";
import { Photos } from "./Photos";

function Header({ title = "Header title" }) {
  return (
    <header>
      <h1 style={{ color: "#999", fontSize: "19px" }}>{title}</h1>
    </header>
  );
}

Header.propTypes = {
  title: PropTypes.string,
};

function Main(props) {
  return <main>{props.children}</main>;
}

Main.propTypes = {
  children: PropTypes.node,
};

function Footer({ title = "Footer title" }) {
  return <footer style={{ color: "#000", fontSize: "12px" }}>{title}</footer>;
}

Footer.propTypes = {
  title: PropTypes.string,
};

function Page() {
  return (
    <Fragment>
      <Header />
      <Main>
        <Photos data={data} />
      </Main>
      <Footer title="Copyright 2023" />
    </Fragment>
  );
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<Page />);
